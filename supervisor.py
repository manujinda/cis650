"""
CIS 650
SPRING 2016
usage: pass_token_mqtt.py <UID> <upstream UID>

> For python mosquitto client $ sudo pip install paho-mqtt
> Command line arg to check status of broker $ /etc/init.d/mosquitto status

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
"""
import sys
import time
import paho.mqtt.client as mqtt
from math import ceil

#############################################
## Get UID and upstream_UID from args
#############################################
if len(sys.argv) != 2:
	print('ERROR\nusage: supervisor.py <int maxval>')
	#print('ERROR\nusage: supervisor.py <int maxval> <int chunk_size>')
	sys.exit()

try:
    maxval = int(sys.argv[1])
    #chunk_size = int(sys.argv[2])
except ValueError:
	print('ERROR\nusage: supervisor.py <int maxval>')
	sys.exit()

UID = "sup"

################
# My Additions #
################
bag = []
chunk_size = 9999
count = 0
task_num = 0
worker_tasks = []   # For keeping track of which worker has which task. So we can do EC. List of pairs in format of (worker_id, task_num)
done_tasks = 0
active_workers = [] # Track which workers are part of this. Can't shut down until all workers are done.
redo_tasks = []

def add_primes(maxval):
    global bag
    i = 2
    while(i <= maxval):
        endval = min(maxval,i+chunk_size)
        bag.append((i,endval))
        i = endval + 1	

def make_range_message(taskNum, rangePair):
    return str(taskNum) + "_" + str(rangePair[0]) + "_" + str(rangePair[1])

# message in format of worker_num, count
def decode_finished_message(msg):
    index = msg.index("_")  # Will throw exception if not found. This is probably ok?
    worker_num = msg[:index]
    count_temp = msg[index + 1:]
    global count
    count += int(count_temp)
    print("Updated count is " + str(count))
    is_removed = False
    global done_tasks
    done_tasks += 1
    global worker_tasks
    for element in worker_tasks:
        if (element[0] == worker_num):
            worker_tasks.remove(element)
            is_removed = True
    if (not is_removed):
        print("Failed to remove task")


#############################################
## MQTT settings
#############################################

broker      = 'green0'
port        = 1883

# publish topics
send_worker_topic = 'worker/'

# subscribe topics
request_topic = 'request/'
finished_topic = 'finished/'
will_topic = 'will/'

#quality of service
qos = 0

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on request_topic
# msg format = (node id)
def on_request(client, userdata, msg):
    print("Received request from " + str(msg.payload))
    global task_num
    global bag
    global worker_tasks
    global active_workers
    global done_tasks
    if (not msg.payload in active_workers):
        active_workers.append(msg.payload)
    if (task_num < len(bag)):
        client.publish(send_worker_topic + str(msg.payload), make_range_message(task_num,bag[task_num]))
        worker_tasks.append((msg.payload,task_num))
        task_num += 1
    elif (done_tasks < len(bag)):
        if (len(redo_tasks) == 0):    # Redo failed tasks once each has been attempted once.
            client.publish(send_worker_topic + str(msg.payload), "wait")
        else:
            task_temp = redo_tasks[0]
            client.publish(send_worker_topic + str(msg.payload), make_range_message(task_temp, bag[task_temp]))
            worker_tasks.append((msg.payload, task_temp))
            redo_tasks.remove(task_temp)
    else:
        client.publish(send_worker_topic + str(msg.payload), "stop")
        active_workers.remove(msg.payload)
        if (len(active_workers) == 0):
            print("This task is ended.")
            client.loop_stop()
            client.disconnect()
        worker_tasks.append((msg.payload,"ended"))

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received will message: " + msg.payload)
    for pair in worker_tasks:
        if (pair[0] == msg.payload and pair[1] != "ended"):
            redo_tasks.append(pair[1])
            active_workers.remove(str(pair[0]))
            worker_tasks.remove(pair)
            print("Task " + str(pair[1]) + " is available for reassignment")
        else:
            print("Unassigned worker: " + msg.payload)

def on_finished(client, userdata, msg):
    print("Received finished message: " + str(msg.payload) + "on topic: " + msg.topic)
    decode_finished_message(msg.payload)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = "Dead supervisor"
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(request_topic, on_request)
    client.message_callback_add(will_topic, on_will)
    client.message_callback_add(finished_topic, on_finished)

    # connect to broker
    client.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    client.subscribe([(request_topic, qos),
                      (will_topic, qos),
                      (finished_topic, qos),
                      ])
    add_primes(maxval)
#    time.sleep(3)
    # initiate pub/sub
#    if UID == 1:
#        time.sleep(5)
#        client.publish(send_token_topic, UID)

    # network loop
    client.loop_forever()

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()
