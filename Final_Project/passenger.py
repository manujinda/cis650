'''nnn
    Passenger request collection
'''

import paho.mqtt.client as mqtt
import time
import csv
import os
import sys

# Track the maximum coordinates that fall within the service area
MAX_X = {1:7, 2:6, 3:6, 4:7}
MAX_Y = {1:9, 2:9, 3:9, 4:9}


#############################################
## Get UID and upstream_UID from args
#############################################

UID = "pass"


#############################################
## MQTT settings
#############################################

broker      = 'brix.d.cs.uoregon.edu'
port        = 8100

# publish topics
pickup_request_topic = 'pickup'

# subscribe topics
will_topic = 'will/'

#quality of service
qos = 0


def validate_coordinates(x, y):
    if x > 0 and y > 0:
        q = 1
    elif x < 0 and y > 0:
        q = 2
    elif x < 0 and y < 0:
        q = 3
    else:
        q = 4
        
    if abs(x) > MAX_X[q] or abs(y) > MAX_Y[q] or abs(x) < 1 or abs(y) < 1 or (abs(x) == 1 and abs(y) == 1):
        print 'Reqesst ( {}, {} ) is outsider our service area'.format(x, y)
        return False

    return True

''' The requests are stored in a CSV file
    Format: delay, x, y 
    Passenger will sleep delay amount of seconds before reading the next request
    The file is named requests.csv and
    it is placed in the same directory where the supervisor code is in'''
def read_requests():
    request_path = os.path.dirname(os.path.realpath(__file__)) + "/requests.csv"

    # Check whether the file exists.
    if not os.path.exists(request_path):
   	print '\nThe data file %s does not exist, exit...' % (request_path)
   	sys.exit()
   	
    with open(request_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            x = int(row['x'])
            y = int(row['y'])
            delay = int(row['delay'])
            
            if (validate_coordinates(x, y)):
                print 'Sending request ( {}, {} ) and sleep for {}'.format(x, y, delay)
                passenger.publish(pickup_request_topic, '{} {}'.format(x, y))
                time.sleep(delay)



##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    #print("Message ID "+str(mid)+ " successfully published")
    return
    
#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("\tReceived will message: " + msg.payload)
    #for pair in worker_tasks:
    #    if (pair[0] == msg.payload and pair[1] != "ended"):
    #        redo_tasks.append(pair[1])
    #        active_workers.remove(str(pair[0]))
    #        worker_tasks.remove(pair)
    #        print("Task " + str(pair[1]) + " is available for reassignment")
    #    else:
    #        print("Unassigned worker: " + msg.payload)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    passenger = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = "Dead passenger"
    passenger.will_set(will_topic, will_message)

    # callbacks
    passenger.on_connect = on_connect
    passenger.on_publish = on_publish
    passenger.on_message = on_message

    # callbacks for specific topics
    passenger.message_callback_add(will_topic, on_will)

    # connect to broker
    passenger.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    passenger.subscribe([(will_topic, qos)
                        ])


    # network loop
    # passenger.loop_forever()
    passenger.loop_start()    # Run one thread in background.
    
except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    passenger.disconnect()

    
# First read anything present in the configuration file
#read_requests()

# Done reading and sending requests in the configuration file
# Now get more requests from standard input
while (True):
    #print 'Enter passenger coordinates : ',
    
    
    x, y = input('Enter passenger coordinates x, y : ')
    
    #if x > 0 and y > 0:
    #    q = 1
    #elif x < 0 and y > 0:
    #    q = 2
    #elif x < 0 and y < 0:
    #    q = 3
    #else:
    #    q = 4
    #    
    #if abs(x) > MAX_X[q] or abs(y) > MAX_Y[q] or abs(x) < 3 or abs(y) < 3:
    #    print 'Reqesst ( {}, {} ) is outsider our service area'.format(x, y)
    #    continue
    
    if (x == 0 and y == 0):
        read_requests()
    elif (validate_coordinates(x, y)):
        print 'Sending pickup coordinates ( {}, {} )'.format(x, y)
        passenger.publish(pickup_request_topic, '{} {}'.format(x, y))
        time.sleep(1)
