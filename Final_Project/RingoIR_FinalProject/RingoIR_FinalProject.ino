#include "RingoHardware.h"
#include "FunStuff.h"
#include "IRCommunication.h"

#define MSG_SIZE 3
byte msg[MSG_SIZE];

byte myID = 0x02; // NOTE: CHANGE THIS VALUE AND THE BELOW VALUE BEFORE RUNNING THIS SCRIPT ON YOUR RINGO. You may also need to adjust motor speeds in MoveToLine and MoveBackToLine so the Ringo doesn't turn after moving.
                  // We might be able to fix this by adding a my_heading global int, and rotating every so often while moving (or just after reaching the destination - rotate back to the original heading.)
int my_int_id = 2;  // Int id = 1 or 2
int rotate_angle = 0;
int leftWhiteVal = 0;
int rightWhiteVal = 0;
int rearWhiteVal = 0;
int leftGreyValLow = 200;
int leftGreyValHigh = 400;
int rightGreyValLow = 200;
int rightGreyValHigh = 400;
int leftDiff;
int rightDiff;
int rearDiff;
// Booleans representing the five phases (TODO: this could be an int or enum)
bool is_awaiting_start_message = false;
int is_get_first_value = 1;
int is_get_second_value = 0;
int is_get_third_value = 0;
int is_get_fourth_value = 0;
bool is_rotate = false;
bool is_move = false;
int elapsed_time = 0;
int wait_time_ms = 500;
int degrinit_new = 0;

bool is_calibrated = false;
float rotationMultiplier;

int i,n,bumpdir,heading,currentheading,degr;
int32_t largenum;
/*
Ringo Robot
Ringo_CalibrateGyroscope_Rev01
Version 1.0 8/2015
The gyroscope on Ringo varies in sensitiviety by about +/- 5% from unit
to unit. For this reason, some navigation functions that use the
gyroscope may be inaccurate by a few degrees. For example, running
the code "RotateAccurate(180, ROTATE_TIMEOUT);" may result in over 
shooting or under shooting the 180 degree mark on your Ringo.
This sketch allows you to calibrate your specific Ringo unit to
account for this inaccuracy.  The calibrated value is stored in
EEPROM in Ringo's processor (so Ringo won't forget the value when
he is turned off). You only need to do this one time for a given
Ringo robot. From that point forward (even if the battery goes dead),
Ringo will automatically read the calibrated value out of his 
memory and use it when navigating.
Instructions for using this sketch:
1) Load this sketch onto your Ringo.
2) Turn on Ringo.  Place him on a smooth surface pointing in any
   specific direction. His rear pixel should be BLUE at this point.
   
3) Press the USER button once and let go. You will hear a quick
   chirp. The rear pixel will then turn GREEN for 1.5 seconds.
   YOU MUST HAVE YOUR HANDS OFF OF RINGO, AND RINGO MUST BE
   COMPLETELY STILL AT THE END OF THIS 1.5 SECONDS. ANY VIBRATION
   ON THE SURFACE WHAT SO EVER WILL EFFECT THE CALIBRATION (including
   fans running on your desk, speakers playing music, etc). You want
   Ringo to be as still as possible at the end of the 1.5 seconds.
4) Ringo's rear light will turn RED and he will begin to spin 
   in 3 consecutive circles. If his gyroscope is perfectly calibrated,
   he will stop heading the exact direction he started at. Chances
   are, he will either over-shoot or under-shoot a bit.
   After he finishes rotating, his rear LED will turn BLUE again.
   Make a mental note of how many degrees he was over or under.
5) Plug Ringo into the programming adaptor and open the serial
   monitor window. Make sure the serial monitor is set to
   "57600 baud" (drop down in the lower right of the serial window).
   Type in the number of degrees Rigno over shot (as a positive
   number) or the number of degrees he under-shot (as a negative
   number).
   
6) Press enter on your computer keyboard. You should hear Ringo beep 
   and see the number you entered echoed back to the screen.
   Once the number is accepted, Ringo does some quick calculations
   in the background and writes the new calibration offset to his
   EEPROM memory.
7) Unplug Ringo from the programming cable and repeat the test.
   The next time he spins he should be more accurate. If Ringo
   is still over or under-shooting, continue the process of plugging
   him back into the programming cable and entering the amount
   of over or under shoot until you are satisfied he is rotating 
   accurately. Note, the rotation function is only accurate 
   to +/- one degree, so he'll never rotate PERFECTLY,
   but he should get pretty close even after 3 full rotations.
8) Press the RESET button (or click the power switch off then
   back on). Without plugging Ringo back into the programmer,
   run the test again and he should still be rotating
   accurately now. If this is true, then you're all set!
   NOTE: The calibration is stored in EEPROM addresses 1020~1024
         which is the very end of the EEPROM address space.
         Be sure to avoid writing to these addresses if you use
         the EEPROM for anything else to avoid over-writing
         the calibration. If the calibration is accidentally
         over-written you can just run this calibration sequence
         again.
This example written by Kevin King, starting from 
Ringo_GyroSenseAndTurn by Dustin Soodak
Portions from other open source projects where noted.
This code is licensed under:
Creative Commons Attribution-ShareAlike 2.0 Generic (CC BY-SA 2.0)
https://creativecommons.org/licenses/by-sa/2.0/
Visit http://www.plumgeek.com for Ringo information.
Visit http://www.arduino.cc to learn about the Arduino.
*/

#include "RingoHardware.h"

#define ROTATE_TIMEOUT 4000
#define ROTATION_DEGREES_FOR_TEST 1080

void setup(){
  HardwareBegin();        //initialize Ringo's brain to work with his circuitry
  PlayStartChirp();       //Play startup chirp and blink eyes
  
  RxIRRestart(4);         //wait for 4 byte IR remote command
  IsIRDone();
  GetIRButton();
  // End Remote Simulation
  ResetIR(MSG_SIZE);
  Serial.begin(9600);
  randomSeed(analogRead(0));
  RestartTimer();
  rotationMultiplier = 1080.0/1110.0;
  Serial.println();
  CalibrateValues();
  OnEyes(0,0,200);
}

char res;
float NewGyroscopeCalibrationMultiplier;
float degr_n=ROTATION_DEGREES_FOR_TEST;
float overshoot=0;
float runningOvershoot=0;
int degrfinal,degrinit;
float rotationActual=ROTATION_DEGREES_FOR_TEST;

void send_completion_message()
{
  msg[0] = myID; // send myID with the message
  msg[1] = myID;  // Again, just because.
  msg[2] = 0;
  for (int i = 0; i < 10; i++)  // Send a total of 10 times over 500 ms.
  {
    SendIRMsg(myID, msg, MSG_SIZE);
    ResetIR(MSG_SIZE);
    delay(50);
  }
}

void send_fail_message()
{
  msg[0] = myID; // send myID with the message
  msg[1] = 0x0F;  // Fail message = 15
  msg[2] = 0;
  for (int i = 0; i < 5; i++)  // Send a total of 10 times over 500 ms.
  {
    SendIRMsg(myID, msg, MSG_SIZE);
    ResetIR(MSG_SIZE);
    delay(50);
  }
}

void CalibrateValues()
{
  int leftSum = 0;
  int rightSum = 0;
  int rearSum = 0;
  for (int i = 0; i < 10; i++)
  {
    ReadLights();
    leftSum += leftDiff;
    rightSum += rightDiff;
    rearSum += rearDiff;
    delay(500);
    /*
    Serial.print("left is: ");
    Serial.println(leftDiff,DEC);
    Serial.print("right is: ");
    Serial.println(rightDiff,DEC);
    */
  }
  leftWhiteVal = (leftSum/10) - 50;
  rightWhiteVal = (rightSum/10) - 50;
  rearWhiteVal = (rearSum/10) - 50;
}

void ReadLights()
{
  int leftOn, rightOn, leftOff, rightOff, rearOn, rearOff;
    digitalWrite(Edge_Lights, HIGH);
    delayMicroseconds(200);
    leftOn = analogRead(LightSense_Left);
    rightOn = analogRead(LightSense_Right);
    rearOn = analogRead(LightSense_Rear);
    digitalWrite(Edge_Lights, LOW);
    delayMicroseconds(200);
    leftOff = analogRead(LightSense_Left);
    rightOff = analogRead(LightSense_Right);
    rearOff = analogRead(LightSense_Rear);
    rearDiff = rearOn - rearOff;
    leftDiff = leftOn - leftOff;
    rightDiff = rightOn - rightOff;
}

bool isBlack(int num, bool left)
{
  if (left)
    return (num < leftGreyValLow);
   else
    return (num < rightGreyValLow);
}

bool isWhite(int num, bool left)
{
  if (left)
    return (num > leftWhiteVal);
   else
    return (num > rightWhiteVal);
}

bool isGrey(int num, bool left)
{
  if (left)
    return (num > leftGreyValLow && num < leftGreyValHigh);
  else
    return (num > rightGreyValLow && num < rightGreyValHigh);
}

void MoveToLine()
{
  int start_time = millis();
  // If on black line, we need to move off the black line.
  if (my_int_id == 1)
    Motors(48,50);
  else
    Motors(44,50);
  delay(100);
  ReadLights();
  if (isBlack(leftDiff, true) || isBlack(rightDiff, false))
  {
    ReadLights();
    while (!(isWhite(leftDiff, true) && isWhite(rightDiff, false)))
    {
      delay(10);
      ReadLights();
    }
  }
  
  // Otherwise, move until we hit a black line or a grey line.
  while (!isBlack(leftDiff, true) && !isBlack(rightDiff,false) && !isGrey(leftDiff,true) && !isGrey(rightDiff,false))
  {
    delay(10);
    ReadLights();
  }
  elapsed_time = millis() - start_time;
  Motors(0,0);
  if (isGrey(leftDiff,true) || isGrey(rightDiff,false))
  {
    delay(500);
    ReadLights();
    if (isGrey(leftDiff,true) || isGrey(rightDiff,false))
      send_fail_message();
  }
}


void MoveBackToLine()
{
  // If on black line, we need to move off the black line.
  if (my_int_id == 1)
    Motors(-51,-52);
  else
    Motors(-45,-51);
  delay(elapsed_time);
  Motors(0,0);
  /*
  delay(100);
  ReadLights();
//  if (isBlack(leftDiff, true) || isBlack(rightDiff, false))
  if (isBlack(rearDiff, true))
  {
    ReadLights();
//    while (!(isWhite(leftDiff, true) && isWhite(rightDiff, false)))
    while (!isWhite(rearDiff,true))
    {
      delay(10);
    ReadLights();
    }
  }
  
  // Otherwise, move until we hit a black line.
//  while (!(isBlack(leftDiff, true)) && !isBlack(rightDiff,false))
  while (!(isBlack(rearDiff,true)))
  {
    delay(10);
    ReadLights();
  }
  Motors(0,0);
  */
}

int convertButtonToInt(int button)
{
//    Serial.print("New button is: ");
//    Serial.println((button - 5 * (my_int_id - 1)),DEC);
  return button - 5 * (my_int_id - 1);  // 0 maps to 0, 4 maps to 4, 5 maps to 0 (for worker 2), 9 maps to 4 (for worker 2).
}

void loop(){
  /* 
  if (!is_calibrated)
  {
    SetPixelRGB(TAIL_TOP,0,0,40);
    SwitchPixelsToButton();
    SwitchMotorsToSerial();
    Serial.print("Present Multiplier: ");Serial.print(GyroscopeCalibrationMultiplier,8);Serial.println("");
    Serial.println("Enter degrees over/undershoot after 3 rotations... ");
    while(!ButtonPressed()){//go in endless loop until button pressed
      if(Serial.available()){
        if(isDigit(Serial.peek()) || Serial.peek()=='-'){
          SetPixelRGB(TAIL_TOP,0,60,0);
          PlayChirp(NOTE_G6,100);delay(100);OffChirp();
          SetPixelRGB(TAIL_TOP,0,0,40); 
          SwitchPixelsToButton();
          overshoot=Serial.parseInt();//note: this will run faster if serial terminal is set to send at least one newline character.
          if(overshoot>180){
            overshoot=180;
          }
          if(overshoot<-180){
            overshoot=-180;
          }
          runningOvershoot+=overshoot;
          Serial.println("");
          Serial.print("Overshoot: ");Serial.print(overshoot,4);Serial.println("");   
          Serial.print("Running Overshoot: ");Serial.print(runningOvershoot,4);Serial.println("");  
  
          rotationActual = ROTATION_DEGREES_FOR_TEST+runningOvershoot;       
          NewGyroscopeCalibrationMultiplier = degr_n/rotationActual;
  
          EEPROM_writeAnything(GYRO_CAL_MULT_ADDRESS, NewGyroscopeCalibrationMultiplier); //write to EEPROM
          GetGyroCalibrationMultiplier();                                                 //go retrieve new value
          is_calibrated = true;
          Serial.print("New Multiplier from EEPROM: ");Serial.print(GyroscopeCalibrationMultiplier,8);Serial.println("");       
        }
        Serial.println();  
        delay(1);//in case the OS delays any characters  
        while(Serial.available()) Serial.read(); 
      }
    }    
    Serial.println();
  
  
    PlayChirp(NOTE_C7,100);delay(100);OffChirp();
    while(ButtonPressed());
    delay(250);
    SetPixelRGB(TAIL_TOP,0,40,0);
    SwitchPixelsToButton();
    delay(1500);
    NavigationBegin();
    degrinit=PresentHeading();
    res=RotateAccurate(degrinit+degr_n, ROTATE_TIMEOUT);
    degrfinal=PresentHeading();
    SetPixelRGB(TAIL_TOP,20,0,0);
    SwitchPixelsToButton();
  }
  */
  
  if (is_rotate)
  {
    OnEyes(50, 0, 50);
    NavigationBegin();
    degrinit_new=PresentHeading();
    rotate_angle = -1 * rotate_angle;
//    Serial.print("Rotation angle is: ");
//    Serial.println(rotate_angle,DEC);
    if (rotate_angle < -180)  // Convert to positive angle.
      rotate_angle = 360 - (rotate_angle * -1);
      
    int res=RotateAccurate(degrinit_new+(rotate_angle*rotationMultiplier), 4000); // TODO UNCOMMENT ME
    int degrfinal=PresentHeading();
    is_rotate = false;
    is_move = true;
    OnEyes(50, 50, 0);
  }

  if (is_move)
  {
    MoveToLine();
    delay(200);
    MoveBackToLine();
   RotateAccurate(degrinit_new, 4000);
   delay(50);
    send_completion_message();
    is_move = false;
    is_get_first_value = 1;
   OnEyes(50, 0, 50);
   // TODO: How do we tell if it is stuck? Can we put a grey (50%) strip near the edge?
  }

  byte button;
  /*
  if (is_awaiting_start_message && IsIRDone())
  {
    button = GetIRButton();
//    Serial.print("Button 0 is: ");
//    Serial.println(button,DEC);
    if (button == my_int_id)
    {
      is_awaiting_start_message = false;
      is_get_first_value = 1;
    }
    else
      delay(1500); // Wait long enough to avoid the next message.
    delay(300);
    ResetIR(MSG_SIZE);
  }
*/
  if(is_get_first_value > 0 && IsIRDone()){                   //wait for an IR remote control command to be received
    button = GetIRButton();       // read which button was pressed, store in "button" variable
    if (button == 10)
      button = 0;
    button = convertButtonToInt(button);
//    Serial.print("Button 1 is: ");
//    Serial.println(button,DEC);
    if (button >= 0 && button <= 4)
    {
      rotate_angle = button;
      is_get_first_value = 0;
      is_get_second_value = 1;
      OnEyes(0, 50, 50);
      delay(wait_time_ms);
      ResetIR(MSG_SIZE);
    }
    else
    {
      delay(wait_time_ms / 5);
      ResetIR(MSG_SIZE);
    }
  }

  if (is_get_second_value > 0 && IsIRDone())
  {
      button = GetIRButton();
      if (button == 10)
        button = 0;
      button = convertButtonToInt(button);
      if (button >= 0 && button <= 4)
      {
  //    Serial.print("Button 2 is: ");
  //    Serial.println(button,DEC);
        rotate_angle *= 5;
        rotate_angle += button;
        is_get_second_value = 0;
        is_get_third_value = 1;
        delay(wait_time_ms);
        ResetIR(MSG_SIZE);
      }
    else
    {
      delay(wait_time_ms / 5);
      ResetIR(MSG_SIZE);
    }
  }

  if (is_get_third_value > 0 && IsIRDone())
  {
      button = GetIRButton();
      if (button == 10)
        button = 0;
      button = convertButtonToInt(button);
      if (button >= 0 && button <= 4)
      {
  //    Serial.print("Button 3 is: ");
  //    Serial.println(button,DEC);
        rotate_angle *= 5;
        rotate_angle += button;
        is_get_third_value = 0;
        is_get_fourth_value = 1;
        delay(wait_time_ms);
        ResetIR(MSG_SIZE);
      }
    else
    {
      delay(wait_time_ms / 5);
      ResetIR(MSG_SIZE);
    }
  }

  if (is_get_fourth_value > 0 && IsIRDone())
  {
      button = GetIRButton();
      if (button == 10)
        button = 0;
      button = convertButtonToInt(button);
      if (button >= 0 && button <= 4)
      {
  //    Serial.print("Button 3 is: ");
  //    Serial.println(button,DEC);
        rotate_angle *= 5;
        rotate_angle += button;
        is_get_fourth_value = 0;
        is_rotate = true;
      delay(wait_time_ms);
        ResetIR(MSG_SIZE);
      }
    else
    {
      delay(wait_time_ms / 5);
      ResetIR(MSG_SIZE);
    }
  }
}
