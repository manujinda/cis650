#!/usr/bin/env python2
"""
precursor commands:

--use these 2--
sudo /etc/init.d/lirc stop
sudo lircd -d /dev/lirc0
-----

sudo killall mode2
sudo /etc/init.d/lirc stop
sudo lircd -d /dev/lirc0
mode2 -d /dev/lirc0

mode2 output:
space xxx
pulse yyy

For IR communication to Ringo robot should receive 4 bytes (32 bits)
1st byte == all 0's
2nd byte == all 1's
3rd,4th == message
"""

from subprocess import Popen, PIPE
import sys
import time
from grovepi import *
import paho.mqtt.client as mqtt
from math import atan2, degrees, pi, cos, sin
import subprocess


LED = 4
pinMode(LED, "output")

# PREAMBLE
beginSend_firstNum = 9000
beginSend_secondNum = 4500
# DATA
data_zeroNum = 600
data_oneNum = 1600
# JUNK
garbage = 20000

# Info for moving Ringo
neighbor = 1    # Other node using ricart-agrawla.
quadrant = 0
worker_id = 2
is_ended = False
is_working = False
x_coord = 0
y_coord = 0
degs = 0
last_angle = 0
req_stamp = 0
privileged = False  # Control access to critical section (drone)
is_waiting_for_drone = False
is_waiting_for_ringo = False
clock = 0
counter = 0
pending = []
#p = Popen(["mode2", "-d", "/dev/lirc0"], stdout=PIPE, bufsize=1)

### Read command line arguments ###
if len(sys.argv) != 3:
    print("len is: " + str(len(sys.argv)))
    print('ERROR\nusage: finalproj_worker.py <int: ID> <int: neighbor>. Sample usage: finalproj_worker.py 1 2')
    sys.exit()

try:
    worker_id = int(sys.argv[1])  # UID is the neighbor's ID (either 1 or 2)
except ValueError:
    print('ERROR\nusage: finalproj_worker.py <int: ID> <int: neighbor>. Sample usage: finalproj_worker.py 1 2')
    sys.exit()

try:
    neighbor =int(sys.argv[2])
except ValueError:
    print('ERROR\nusage: finalproj_worker.py <int: ID> <int: neighbor>. Sample usage: finalproj_worker.py 1 2')
    sys.exit()


### Button ###
button = 3

pinMode(button,"INPUT")
print("button: " + str(digitalRead(button)))
#################
### MAIN CODE ###
#################
def wait_for_switch():
    first = 0
    second = 0
    while (not (first == 1 and second == 1)):
        first = digitalRead(button)
        time.sleep(0.1)
        second = digitalRead(button)


def processLine(line):
    reading = line.split()
    reading[1] = int(reading[1])
    if reading[0] == "space":
        return durToByte(reading[1])
    elif reading[0] == "pulse":
        return durToByte(reading[1])
    else:
        sys.exit()

def durToByte(duration):
    dct = {}
    dct["Preamble1"] = abs(beginSend_firstNum  - duration)
    dct["Preamble2"] = abs(beginSend_secondNum  - duration)
    dct["0"] = abs(data_zeroNum - duration)
    dct["1"] = abs(data_oneNum - duration)
    dct["garbage"] = abs(garbage- duration)
    return min(dct, key=dct.get)

def wait_for_ringo():
    print("Waiting for response from Ringo")
    binary = ''
    message = ''
    is_waiting = True
    count = 0
    p = Popen(["mode2", "-d", "/dev/lirc0"], stdout=PIPE, bufsize=1)
    while (is_waiting):
        with p.stdout:
            for line in iter(p.stdout.readline, b''):  # b'' denotes a byte string literal
                # print line,
                line = processLine(line)
                if line == "Preamble2":  # start keeping track of durations
                    binary = ''
                    message = ''
                    count = 0
                try:
                    binary += str(int(line))
                except:
                    print(line)

                # space and duration = 2
                if len(binary) == 2:
                    message = str(int(binary, 2)) + message
                    binary = ''

                # 1 byte = 8 bits
                if len(message) == 8:
                    if '2' in message:
                        print("ERROR")
                    else:
                        try:
                            print("msg = " + str(int(message,2)))
                            if (count == 0 and int(message,2) == worker_id):  # This is my worker, so it must be done moving.
                                is_waiting = False
                                count += 1
                            elif (count == 1 and not is_waiting):
                                if (int(message,2) == 15):
                                    print("Worker is stuck! Please reposition the worker, and then hit the switch to continue.")
                                    wait_for_switch()
                                p.terminate()
                            message = ''
                        except:
                            print("invalid message")
        p.wait()
        global is_working
        is_working = False
        is_waiting = False

def send_key(val):
    if (val == 10):
        val = 0
        print("Incorrect value " + str(val))
    if (worker_id == 2):
        val += 5
    # No idea why using Popen and then terminating is necessary, but it is. Maybe.
    p = Popen(["mode2","-d", "/dev/lirc0"], stdout=PIPE,bufsize=1)
    with p.stdout:
        p.terminate()
    p.wait()

    # Now we actually send the signal
    print("sending KEY_" + str(val))
    subprocess.Popen(("irsend", "SEND_START", "Ringo", "KEY_" + str(val)))
    time.sleep(0.5)
    subprocess.Popen(("irsend", "SEND_STOP", "Ringo", "KEY_" + str(val)))

def send_angle(degrees):
    print("sending angle " + str(degrees))
    digitalWrite(LED, 1)
    send_key(int(degrees / 125))
    send_key(int(degrees / 25) % 5)
    send_key(int((degrees / 5) % 5))
    send_key(int(degrees % 5))
    digitalWrite(LED, 0)
    global is_waiting_for_ringo
    is_waiting_for_ringo = True

def send_pickup_request(quad):
    global privileged
    global req_stamp
    global pending
    print("---Pickup request for drone: Move vehicle " + str(worker_id) + " to quadrant " + str(quad))
    wait_for_switch()
    send_angle(degs)
    for p in pending:
        send_permission_message(p, id)
    pending = []
    privileged = False
    req_stamp = 0

def read_coord_message(client, msg):
    global x_coord
    global y_coord
    global degs
    global quadrant
    global last_angle
    last_angle = 0 # 6/4/2016 - Ringo resets angle after returning to origin.

    # Read coordinates
    index = msg.index("_")
    index2 = msg[index+1:].index("_") + index+1
    new_quadrant = int(msg[:index])
    x_coord_new = int(msg[index+1:index2])
    y_coord_new = int(msg[index2+1:])
    x_init = 1  # Initial value of x coordinate
    y_init = 1  # Initial value of y coordinate

    # Calculate new quadrant
    """
    if (x_coord >= 0 and y_coord >= 0):
        new_quadrant = 1    # Upper right
    elif (x_coord >= 0 and y_coord < 0):
        new_quadrant = 2    # Lower right
        y_coord = y_coord * -1
    elif (x_coord < 0 and y_coord < 0):
        new_quadrant = 3    # Lower left
        y_coord = y_coord * -1
        x_coord = x_coord * -1
    elif (x_coord < 0 and y_coord >= 0):
        new_quadrant = 4
        x_coord = x_coord * -1
    else:
        print("Error calculating quadrant for " + str(x_coord) + "," + str(y_coord))
    """
    # Calculate angle - following atan2 concept from http://stackoverflow.com/questions/10473930/how-do-i-find-the-angle-between-2-points-in-pygame
    if (new_quadrant == quadrant):
        # calculate angle from last coordinate + last angle
        x_diff = (x_coord_new - x_coord)
        y_diff = (y_coord_new - y_coord)
        rads = atan2(y_diff, x_diff)
        degs = degrees(rads)
        if (new_quadrant == 2 or new_quadrant == 4):
            degs = degs - (90 - last_angle)
        else:
            degs = degs - last_angle
    else:
        x_diff = (x_coord_new - x_init)
        y_diff = (y_coord_new - y_init)
        rads = atan2(y_diff, x_diff)
        degs = degrees(rads)

    if (new_quadrant == 2 or new_quadrant == 4):
        if (new_quadrant != quadrant):
            degs = 90 - degs    # For these two quadrants, send the adjusted angle.
        else:   # This is not the initial deploy, so switch the angle with its negative
            degs = - degs   # Test this code... but I think angles are reversed in these quadrants
#            degs = degs

    while (degs < 0):
        degs = 360 + degs
    while (degs > 360):
        degs = degs - 360
    # Send angle to ringo, or send pickup request.
    print("---Please place a passenger in quadrant " + str(new_quadrant) + " at coords (" + str(x_coord_new) + "," + str(y_coord_new) + ")")

    if (new_quadrant == quadrant):
        time.sleep(1)
        wait_for_switch()
        send_angle(degs)
    else:
        global privileged
        global is_waiting_for_drone
        privileged = False
        is_waiting_for_drone = True
        get_critical()

    if (new_quadrant == quadrant):
        last_angle = degs + last_angle
    else:
        last_angle = degs
    quadrant = new_quadrant
    # Adjust the x_coord and y_coord value by the angle and half-bug-width (about 1.5)
    x_coord_adj = 1.5 * sin(degs)
    y_coord_adj = 1.5 * cos(degs)
    x_coord = x_coord_new + x_coord_adj
    y_coord = y_coord_new + y_coord_adj
    # Revised logic: Ringo always goes back to origin.
    x_coord = x_init
    y_coord = y_init

######################
### Ricart Agrawla ###
######################
def on_permission(client, userdata, msg):
    global clock
    global counter
    global privileged
    clock += 1
    counter += 1
    if counter == 1:
        counter = 0
        privileged = True

# Send permission message in form of "p_{id}"
def send_permission_message(neighbor_id, my_id):
    client.publish(send_permission_topic, str(my_id))

def on_resource(j,time):
    global clock
    global counter
    global pending
    print("received resource msg " +str(clock) + "_"+str(time))
    if (time > clock):
        clock = time + 1
    else:
        clock += 1
    if (not privileged) and (req_stamp == 0 or (time,j) < (req_stamp,worker_id)): #(time < req_stamp) or (time == req_stamp and j < id))
        send_permission_message(j, worker_id)
    else:
        pending.append(j)


# Resource message has the format workerid_workerclock
def on_resource_msg(client, userdata, msg):
    print("received resource msg " + str(msg.payload))
    msg_payload = msg.payload
    index = msg_payload.index("_")
    neighbor_id = int(msg_payload[:index])
    clock_val = int(msg_payload[index + 1:])
    on_resource(neighbor_id,clock_val)

def get_critical():
    global req_stamp    # Do we need these?
    global counter    # Do we need these?
    req_stamp = clock  # Remember this value in case clock changes. For processing future requests.
    counter = 0
    print("Sending resource request to " + str(get_drone_topic))
    client.publish(get_drone_topic,str(worker_id) + "_" + str(req_stamp))

#############################################
## MQTT settings
#############################################

broker      = 'brix.d.cs.uoregon.edu'
port        = 8100

# publish topics
finished_topic = 'finished/'
request_topic = 'get_request/'
get_drone_topic = 'get_resource/' + str(neighbor)
send_permission_topic = 'send_permission/' + str(neighbor)

# subscribe topics
my_worker_topic = 'request/' + str(worker_id)
my_permission_topic = 'send_permission/' + str(worker_id)
my_resource_topic = 'get_resource/' + str(worker_id)
will_topic = 'will/'

#quality of service
qos = 0

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on my_worker_topic
# msg format = "X_Y" (e.g. "1_3", "-3_-2")
def on_worker(client, userdata, msg):
    print("Received worker message " + str(msg.payload))
    global is_ended
    global is_working
    if (msg.payload == "stop"):
        is_ended = True
        print("Ended")
    elif (msg.payload == "wait"):
        is_working = False
        time.sleep(3) # Request a new message
    else:
        read_coord_message(client,msg.payload)

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')

def on_disconnect(client, userdata, msg):
    print("Disconnected. Reconnecting.")
    client.connect(broker, port, keepalive=3000)

#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(worker_id), clean_session=True)

    # setup will for client
    will_message = str(worker_id)
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(my_worker_topic, on_worker)
    client.message_callback_add(my_permission_topic, on_permission)
    client.message_callback_add(will_topic, on_will)
    client.message_callback_add(my_resource_topic, on_resource_msg)

    # connect to broker
    client.connect(broker, port, keepalive=3000)

    # subscribe to list of topics
    client.subscribe([(my_worker_topic, qos),
                      (will_topic, qos),
                      (my_permission_topic,qos),
                      (my_resource_topic,qos),
                      ])
#    print("my resource topic = " + my_resource_topic)
    client.loop_start()
except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()

###################
#### Main Code ####
###################
time.sleep(5) # Allow 5 seconds for other nodes to start.
while (not is_ended):
    if (not is_working):
        is_working = True
        print("Requesting new pickup for: " + str(quadrant) + " " + str(worker_id))
        client.publish(request_topic, str(quadrant) + " " + str(worker_id))
    elif (is_waiting_for_drone and privileged):
        send_pickup_request(quadrant)
        is_waiting_for_drone = False
    elif (is_waiting_for_ringo):
        wait_for_ringo()
        is_waiting_for_ringo = False
    else:
        time.sleep(1)

