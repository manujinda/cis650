"""nnnn
CIS 650
SPRING 2016
usage: pass_token_mqtt.py <UID> <upstream UID>

> For python mosquitto client $ sudo pip install paho-mqtt
> Command line arg to check status of broker $ /etc/init.d/mosquitto status

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
"""
import sys
import time
import paho.mqtt.client as mqtt
import csv
import os
from collections import deque

#############################################
## Get UID and upstream_UID from args
#############################################

UID = "sup"

################
# My Additions #
################

# Maximum time we make a passenger wait till picked up.. sort of
# Ideally this should be time_of_pickup - time_of_request < SERVICE_DELAY
# But it is not very eash to calculate the time it takes Ringo to travel
# from its current location to the pickup location
# At the moment service delay only reflects the dealy between time_of_request
# till the time_request_sent_to_pi
SERVICE_DELAY = 60

# Requests lists for each qudrant
qudrants = { 1:deque([]), 2:deque([]), 3:deque([]), 4:deque([]) }

# Track the maximum coordinates that fall within the service area
MAX_X = {1:7, 2:6, 3:6, 4:7}
MAX_Y = {1:9, 2:9, 3:9, 4:9}

# keep track of worker locations
workers = {0:0, 1:0, 2:0}   # worker_id:qudrant - 0 means worker is in garage
worker_locations = {0:(0, 0), 1:(0, 0), 2:(0, 0)}
worker_request_times = {}
service_times = []   # List of times that to to serve each request
#idle_workers = set([0, 1, 2])    # initially all the workers are idle
idle_workers = set([])    # initially workers has to inform supervisor that they are ready to work


def send_pickup_request_to_worker(worker_id, worker_qudrant):
    qudrant_to_send = get_next_request(worker_id, worker_qudrant)

    # Update worker state    
    workers[worker_id] = qudrant_to_send   # This worker will go into serve this qudrant

    if qudrant_to_send > 0: # We have some passenger waiting to be picked up
        # Update worker state
        idle_workers.discard(worker_id)       # this worke is no longer idle
        
        pickup_coords = qudrants[qudrant_to_send].popleft()
        workload_msg = prepare_workload_message(qudrant_to_send, pickup_coords)
        worker_locations[worker_id] = (pickup_coords[1], pickup_coords[2])
    
        worker_request_times[worker_id] = time.time()
        print 'Sending worker {} to pickup at ( {} , {} )'.format(worker_id, pickup_coords[1], pickup_coords[2])
        
    else:
        workload_msg = 'wait' 
        # Update worker state
        #idle_workers.add(worker_id)       # this worke will be idle
        #worker_locations[worker_id] = (0, 0)
        #print 'No passengers. Tell worker to wait'
                
        
    supervisor.publish(send_worker_topic + str(worker_id), workload_msg)
    #print 'Sent workload message: ' + workload_msg

        

def add_pickup_request_to_queue(request_time, x, y):
    if x > 0 and y > 0:
        q = 1
    elif x < 0 and y > 0:
        q = 2
    elif x < 0 and y < 0:
        q = 3
    else:
        q = 4
        
    if abs(x) > MAX_X[q] or abs(y) > MAX_Y[q] or abs(x) < 1 or abs(y) < 1 or (abs(x) == 1 and abs(y) == 1):
        print 'Reqesst ( {}, {}, {} ) is outsider our service area'.format(request_time, x, y)
        return

    #qudrants[q].append((request_time, abs(x), abs(y))) 
    qudrants[q].append((request_time, x, y)) 
    
    # if there is an idel worker, send this request to that worker
    if len(idle_workers) > 0:
        worker = idle_workers.pop()
        send_pickup_request_to_worker(worker, workers[worker])
        print 'Worker {} was idel. Sent the request just receied: ( {}, {}, {} )'.format(worker, q, x, y)
    

''' The requests are stored in a CSV file
    Format: arrival_delay, x, y 
    The file is named requests.csv and
    it is placed in the same directory where the supervisor code is in'''
def read_requests():
    request_path = os.path.dirname(os.path.realpath(__file__)) + "/requests.csv"

    # Check whether the file exists.
    if not os.path.exists(request_path):
   	print '\nThe data file %s does not exist, exit...' % (request_path)
   	sys.exit()
   	
    now = time.time() # Current time
    with open(request_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            x = int(row['x'])
            y = int(row['y'])
            delay = int(row['delay'])
            

            add_pickup_request_to_queue(now + delay, x, y)
                        
#            if x > 0 and y > 0:
#                q = 1
#            elif x < 0 and y > 0:
#                q = 2
#            elif x < 0 and y < 0:
#                q = 3
#            else:
#                q = 4
#                
#            if abs(x) > MAX_X[q] or abs(y) > MAX_Y[q] or abs(x) < 3 or abs(y) < 3:
#                print 'Reqesst ( {}, {}, {} ) is outsider our service area'.format(delay, x, y)
#                continue
#
#            qudrants[q].append((now + delay, abs(x), abs(y))) # now + delay to simulate timed arrival of requests
            
    #print qudrants
    print 1, qudrants[1]
    print 2, qudrants[2]
    print 3, qudrants[3]
    print 4, qudrants[4]

''' Get the maximum waiting request form all of the 4 qudrants'''
def get_max_waiting_qudrant(worker_id):
    other_workers_qudrants = set(workers.values())
    other_workers_qudrants.discard(workers[worker_id])
    other_workers_qudrants.discard(0)
    
    max_wait_qudrant = 0
    earlist_pickup_request_time = 0
    for q in range(1,5):
        if len(qudrants[q]) > 0 and qudrants[q][0][0] > earlist_pickup_request_time and not q in other_workers_qudrants:
            max_wait_qudrant = q
            earlist_pickup_request_time = qudrants[q][0][0]
            
    return max_wait_qudrant, earlist_pickup_request_time
            

'''Prepare the request message'''
def prepare_workload_message(qudrant, request):
    #if ( qudrant == 1 or qudrant == 3):
    #    return '{}_{}_{}'.format(qudrant, abs(request[1]), abs(request[2]))
    #else:
    #    return '{}_{}_{}'.format(qudrant, abs(request[2]), abs(request[1]))
    return '{}_{}_{}'.format(qudrant, abs(request[1]), abs(request[2]))

''' Find the next request to be sent to the worker'''
def get_next_request(worker_id, qudrant):
    now_time = time.time()
    
    other_workers_qudrants = set(workers.values())
    other_workers_qudrants.discard(workers[worker_id])
    other_workers_qudrants.discard(0)
    
    max_wait_qudrant, earlist_pickup_request_time = get_max_waiting_qudrant(worker_id)
    #workload_msg = ''
    
    qudrant_to_send = 0
    #request_to_sent = (0, 0, 0)
    
    if max_wait_qudrant > 0: # We have some passenger waiting to be picked up
        max_wait_time = now_time - earlist_pickup_request_time
        if max_wait_time > SERVICE_DELAY:
            print 'We must serve this passenger in qudrant {} who is waiting for {}'.format(max_wait_qudrant, max_wait_time)
            qudrant_to_send = max_wait_qudrant
            
        elif qudrant != 0 and len(qudrants[qudrant]) > 0: # We have a passenger in the waiting in the same qudrant as the request qudrant
            qudrant_to_send = qudrant
            
        else: # There is a passenger waiting in some other qudrant. Pickup the passenger who was waiting the most
            qudrant_to_send = max_wait_qudrant
            
        #workload_msg = prepare_workload_message(qudrant_to_send, qudrants[qudrant_to_send].popleft())

    if qudrant_to_send in other_workers_qudrants:
        return 0
    else:    
        #return workload_msg
        return qudrant_to_send


#############################################
## MQTT settings
#############################################

broker      = 'brix.d.cs.uoregon.edu'
port        = 8100

# publish topics
send_worker_topic = 'request/'

# subscribe topics
request_topic = 'get_request'
finished_topic = 'finished/'
will_topic = 'will/'
pickup_request_topic = 'pickup'

#quality of service
qos = 0

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    #print("Message ID "+str(mid)+ " successfully published")
    return

#Called when message received on request_topic
# msg format = (node id)
def on_request_from_worker_pi(client, userdata, msg):
    print("Received request from " + str(msg.payload))
    
    # Decode the request message
    # request[0] - present qudrant where the worker is in
    # request[1] - worker id
    request = [int(s) for s in str.split(msg.payload) if s.isdigit()]
    worker_qudrant = request[0]
    worker_id = request[1]
    
    #print request
    
    current_request_time = time.time()
    previous_request_time = worker_request_times.get(worker_id, -1)
    if previous_request_time != -1:
        time_to_serve = current_request_time - worker_request_times[worker_id]
        service_times.append(time_to_serve)
        #print 'Time taken by workder {} to serve pervious request {}'.format(worker_id, time_to_serve)
    
    send_pickup_request_to_worker(worker_id, worker_qudrant)

    #print 'Got request from qudrant {} by worker {}'.format(request[0], request[1])
    
#    qudrant_to_send = get_next_request(worker_qudrant)
#
#    # Update worker state    
#    workers[worker_id] = qudrant_to_send   # This worker will go into serve this qudrant
#
#    if qudrant_to_send > 0: # We have some passenger waiting to be picked up
#        # Update worker state
#        idle_workers.discard(worker_id)       # this worke is no longer idle
#        
#        workload_msg = prepare_workload_message(qudrant_to_send, qudrants[qudrant_to_send].popleft())
#    
#    else:
#        workload_msg = 'wait' 
#        # Update worker state
#        idle_workers.add(worker_id)       # this worke will be idle
#                
#        
#    supervisor.publish(send_worker_topic + str(worker_id), workload_msg)
#    print 'sent message: ' + workload_msg
    

def on_pickup_request_from_passenger(client, userdata, msg):
    now_time = time.time()
    
    # Decode the request message
    # request[0] - pickup x coordinate
    # request[1] - pickup y coordinate
    #request = [int(s) for s in str.split(msg.payload) if s.isdigit()]
    request = [int(s) for s in str.split(msg.payload)]
    
    if len(request) > 1:
        x = request[0]
        y = request[1]
    else:
        print 'Invalid pickup request'
        return
    
    print 'Pickup request ( {}, {}, {} )'.format(now_time, x, y)
    add_pickup_request_to_queue(now_time, x, y)

    #print 1, qudrants[1]
    #print 2, qudrants[2]
    #print 3, qudrants[3]
    #print 4, qudrants[4]


#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("\tReceived will message: " + msg.payload)
    #for pair in worker_tasks:
    #    if (pair[0] == msg.payload and pair[1] != "ended"):
    #        redo_tasks.append(pair[1])
    #        active_workers.remove(str(pair[0]))
    #        worker_tasks.remove(pair)
    #        print("Task " + str(pair[1]) + " is available for reassignment")
    #    else:
    #        print("Unassigned worker: " + msg.payload)

def on_finished(client, userdata, msg):
    print("Received finished message: " + str(msg.payload) + "on topic: " + msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    supervisor = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = "Dead supervisor"
    supervisor.will_set(will_topic, will_message)

    # callbacks
    supervisor.on_connect = on_connect
    supervisor.on_publish = on_publish
    supervisor.on_message = on_message

    # callbacks for specific topics
    supervisor.message_callback_add(request_topic, on_request_from_worker_pi)
    supervisor.message_callback_add(will_topic, on_will)
    supervisor.message_callback_add(finished_topic, on_finished)
    supervisor.message_callback_add(pickup_request_topic, on_pickup_request_from_passenger)
    
    # connect to broker
    supervisor.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    supervisor.subscribe([(request_topic, qos),
                        (will_topic, qos),
                        (finished_topic, qos),
                        (pickup_request_topic, qos)
                        ])


    #read_requests()
    
    # network loop
    supervisor.loop_forever()

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    supervisor.disconnect()
