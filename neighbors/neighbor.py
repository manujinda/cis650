"""
CIS 650
SPRING 2016

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
"""
import sys
import time
import paho.mqtt.client as mqtt
from math import ceil
import random, decimal

#############################################
## Get UID and upstream_UID from args
#############################################

if len(sys.argv) != 2:
	print('ERROR\nusage: neighbor.py <int: ID> ')
	sys.exit()

try:
    UID = int(sys.argv[1])  # UID is the neighbor's ID (either 1 or 2)
except ValueError:
    print('ERROR\nusage: neighbor.py <int: ID> ')
    sys.exit()

###############################
# Class Variables & Functions #
###############################
is_initialized = False
initialization_count = 0
got_vars = False
flag = [0, 0, 0]
turn = 0
got_vars_c = 0

# Get the neighbor's UID from mine.
def get_neighbor_id(uid):
    return (uid + 2) % 2 + 1


# Wait for all initialization variables (flag1, flag2, turn) to be received before beginning.
def inc_initialize_count():
    global is_initialized
    global initialization_count
    if (not is_initialized):
        initialization_count += 1
        if (initialization_count >= 3):
            is_initialized = True

# Set the value of a flag to either true or false.
def set_flag(flag_num, message_str):
    global flag
    if (message_str[:1] == "0"):
        flag[flag_num] = False
    elif (message_str[:1] == "1"):
        flag[flag_num] = True
    else:
        print("Unrecognized flag value: " + message_str)

# Set the value of the turn (1 or 2, should match farmer ID)
def set_turn(turn_id):
    global turn
    turn = int(turn_id)

# variables in order of Flag1, Flag2, Turn
def set_vars(message_str):
    global flag
    global turn
    flag[1] = int(message_str[:1])
    flag[2] = int(message_str[1:2])
    turn = int(message_str[2:3])

#############################################
## MQTT settings
#############################################

broker      = 'green0'
port        = 1883

# publish topics
enter_topic = 'enter'
exit_topic = 'exit'
my_setflag_topic = 'setflag/' + str(UID)
other_setflag_topic = 'setflag/' + str(get_neighbor_id(UID))
setturn_topic = 'setturn'
get_vars_topic = 'getvars'

# subscribe topics
flag1_topic = 'flag/1'
flag2_topic = 'flag/2'
turn_topic = 'turn'
will_topic = 'will/'
my_vars_topic = 'sendvars/' + str(UID)

#quality of service
qos = 2

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on one of the topics:
def on_flag1(client, userdata, msg):
    inc_initialize_count()
#    set_flag(1,str(msg.payload))
#    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

def on_flag2(client, userdata, msg):
    inc_initialize_count()
#    set_flag(2,str(msg.payload))
#    print("Received message: " + str(msg.payload) + " on topic: " + msg.topic)

def on_turn(client, userdata, msg):
    inc_initialize_count()
#    set_turn(str(msg.payload))
#    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

def on_vars(client, userdata, msg):
    global got_vars
    got_vars = True
    set_vars(str(msg.payload))
    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+" on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = "Dead UID: {}".format(UID)
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(flag1_topic, on_flag1)
    client.message_callback_add(flag2_topic, on_flag2)
    client.message_callback_add(turn_topic, on_turn)
    client.message_callback_add(my_vars_topic, on_vars)
    client.message_callback_add(will_topic, on_will)

    # connect to broker
    client.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    client.subscribe([(flag1_topic, qos),
                      (flag2_topic, qos),
                      (turn_topic, qos),
                      (will_topic, qos),
                      (my_vars_topic, qos),
                      ])

#    client.loop_forever()
    client.loop_start()    # Run one thread in background.

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()

is_test = False
# Main logic for the Neighbor here
while(True):
#    global UID
#    global is_test
#    global is_initialized

    if (not is_initialized):
        continue    # Don't do anything until initialized!
    if (not is_test):
        # TODO: Add a random chance to enter the is_test state instead of doing the below.
        time.sleep(random.randint(1,2) + decimal.Decimal(random.randrange(100))/100)
        is_test = True
        client.publish(setturn_topic, str(get_neighbor_id(UID)))  # Set turn to neighbor's.
        client.publish(my_setflag_topic, "1")  # Set my flag to true.
        client.publish(get_vars_topic, str(UID))  # Get variables.
        got_vars_c += 1
    elif (is_test and got_vars):    # Check the current state of the variables before entering the field.
        got_vars_c -= 1
        got_vars = False
        if (not flag[get_neighbor_id(UID)] or turn == UID):
            print ("I am entering the field.")
            client.publish(enter_topic, str(UID))  # Publish enter message
#            time.sleep(1)  # Spend some time picking berries. (Also stall so broker doesn't get overloaded).
            client.publish(exit_topic, str(UID))  # Publish exit message.
            client.publish(my_setflag_topic, "0")  # Set my flag to false.
            is_test = False
    elif (not got_vars and got_vars_c == 0):
        time.sleep(1)# wait a bit before checking the variables again. So we dont overload the pipe.
        client.publish(get_vars_topic, str(UID))  # Get variables.
        got_vars_c += 1

