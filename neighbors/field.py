"""
CIS 650
SPRING 2016

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
# Alternative implementation, without global variables (using only callbacks):

# Implementing field, based on the implementatio of the neighbor 

Implementation of the field based on the implementation of the neighbor
"""
import sys
import time
import paho.mqtt.client as mqtt

#############################################
## Get Command line arguments
#############################################

# None needed

###############################
# Class Variables & Functions #
###############################

# Keeps track of who is in the field
# initially nobody is in the field
# a dictionary of the form
# farmer ID -> # of cosecutive entries
infield = {}  

UID = 'field'

entrance_count = [0, 0, 0]

###########################################
# Main logic for the Neighbor below here  #
###########################################

# Field does not need any

#############################################
## MQTT settings
#############################################

broker      = 'green0'
port        = 1883

# subscribe topics
enter_topic = 'enter'
exit_topic = 'exit'
will_topic = 'will/'

#quality of service
qos = 0

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
#def on_publish(client, userdata, mid):
#    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on token_topic
def on_enter(field, userdata, msg):
    global entrance_count
    
    entrance_count[int(msg.payload)] += 1
    
    print("Received message: " + str(msg.payload) + " on topic: " + msg.topic)
    print("Farmer: " + str(msg.payload) + " is entering the field" + str(entrance_count[1:]))
    
    # A dictionary with entries farmer -> # of entries
    # Can keep track of who is in the field and whehter
    # somebody has entered more than once (without exiting)
    infield[msg.payload] = infield.get(msg.payload, 0) + 1
    
    if (infield[msg.payload] > 1):
        # This farmer has enterd the field more than once without exiting in between
        print("ERROR: Farmer: " + str(msg.payload) + " has entered the field " + str(infield[msg.payload]), " times!")
        
    # Total number of unique farmers in the field
    # This does not count mumtiple consecutive enteris
    # (without exiting) by a single farmer
    tot_infield = len(infield)  
    if (tot_infield > 1):
        print("ERROR: " + str(tot_infield) + " farmers are in field at once!")
        
        print("Farmers | "),
        for f in infield.keys():
            print(str(f) + " | "),
        print("are in field")



def on_exit(field, userdata, msg):
    print("Received message: " + str(msg.payload) + " on topic: " + msg.topic)
    print("Farmer: " + str(msg.payload) + " is exiting the field")
    
    if (infield.get(msg.payload, 0) == 0):
        print("ERROR: Farmer: " + str(msg.payload) + " is exiting without entering the field!")
        
    else:
        infield[msg.payload] -= 1
        
        if (infield[msg.payload] == 0):
            del infield[msg.payload]

        
#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: " + str(msg.payload) + " on topic: " + msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: " + str(msg.payload) + " on topic: " + msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    field = mqtt.Client(UID, clean_session=True)

    # reinitialize the client as if it just started
    # remove memorized stuff from the client side
    field.reinitialise(UID, clean_session=True)

    # setup will for client
    will_message = "Dead UID: {}".format(UID)
    field.will_set(will_topic, will_message)

    # callbacks
    field.on_connect = on_connect
    #field.on_publish = on_publish
    field.on_message = on_message

    # callbacks for specific topics
    field.message_callback_add(enter_topic, on_enter)
    field.message_callback_add(exit_topic, on_exit)
    field.message_callback_add(will_topic, on_will)

    # connect to broker
    field.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    field.subscribe([(enter_topic, qos),
                      (exit_topic, qos),
                      (will_topic, qos),
                      ])

    field.loop_forever()

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    field.disconnect()
