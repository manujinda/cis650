"""
CIS 650
SPRING 2016
usage: pass_token_mqtt.py <UID> <upstream UID>

> For python mosquitto client $ sudo pip install paho-mqtt
> Command line arg to check status of broker $ /etc/init.d/mosquitto status

Adapted from provided pass_token_mqtt_v2.py file.
Solution for Green team (Roger, Troy, and Manujinda)
"""
import sys
import time
import paho.mqtt.client as mqtt
from math import ceil
from multiprocessing import Pool, current_process, cpu_count, active_children
from functools import partial, reduce

#############################################
## Get UID and upstream_UID from args
#############################################

if len(sys.argv) != 2:
	print('ERROR\nusage: worker.py <int id>')
	sys.exit()

try:
    UID = int(sys.argv[1])
except ValueError:
	print('ERROR\nusage: worker.py <int id>')
	sys.exit()



################
# My Additions #
################
is_ended = False
is_working = False
is_waiting = False

def is_prime(n):
    if n < 2:
        return False
    elif n == 2:
        return True
    for i in range (2,int(ceil(n**0.5))+1):
        if n%i == 0:
            return False
    return True

def chunk_range(startval, endval, num_chunks):  # num_chunks = number of chunks, points = list of points
    chunk_size = int((endval-startval) / num_chunks)
    #if (chunk_size < 1):
    #    return [(startval, endval)]
    lower = startval
    upper = min(lower + chunk_size,endval)
    chunk_list = [(lower,upper)]
    i = 1
    while (i < num_chunks):
        lower = upper
        upper = min(lower + chunk_size, endval)  # Min of (upper+chunk_size and n)?
        if (i == num_chunks - 1):
            upper = endval
        chunk_list.append((lower,upper))
        i += 1
    #chunk_list = [(startval,endval)]
    return chunk_list

def count_primes_small(startnum,endnum):
    count = 0
    for i in range(startnum,endnum):
        if is_prime(i):
            count += 1
    #print("Primes in the range {} - {} = {}".format(startnum, endnum, count))
    return count

def count_primes_lambda(pair_list):
#    counts = map(lambda pair: (count_primes_small(pair[0],pair[1])), pair_list)
    counts = count_primes_small(pair_list[0],pair_list[1])
    return counts
#    return reduce(lambda x,y:x+y,counts,0)

def count_primes(client,task_num_str,startnum,endnum):
    chunks = chunk_range(startnum, endnum, 100)
    processor_queue_size = ((endnum-startnum)/100)
    counts = pool.map(count_primes_lambda, chunks, processor_queue_size)
    count = reduce(lambda x,y:x+y,counts,0)
    client.publish(finished_topic,str(UID)+"_"+str(count))
    #TODO: Do we need to sleep here? Will messages arrive at supervisor in wrong order if not?
    global is_working
    is_working = False


# Msg in format of tasknum_rangepair0_rangepair1
def read_range_message(client, msg):
    index = msg.index("_")
    index2 = msg[index+1:].index("_")
    task_num_str = msg[:index]
    startnum = int(msg[index+1:index+1+index2])
    endnum = int(msg[index+1+index2+1:])
    count_primes(client,task_num_str,startnum,endnum)


##############
# Start pool #
##############
def start_process():
    print( 'Starting {} with pid {}'.format(current_process().name,current_process().pid)) #delayed print from when pool initialized
    return

if __name__ == '__main__':  # Prevent Windows issues.
    pool = Pool(processes=3,
                initializer=start_process,
               )

#############################################
## MQTT settings
#############################################

broker      = 'green0'
port        = 1883

# publish topics
finished_topic = 'finished/'
request_topic = 'request/'

# subscribe topics
my_worker_topic = 'worker/' + str(UID)
will_topic = 'will/'

#quality of service
qos = 0

##############################################
## MQTT callbacks
##############################################

#Called when the broker responds to our connection request
def on_connect(client, userdata, flags, rc):
    if rc != 0:
        print("Connection failed. RC: " + str(rc))
    else:
        print("Connected successfully with result code RC: " + str(rc))

#Called when a published message has completed transmission to the broker
def on_publish(client, userdata, mid):
    print("Message ID "+str(mid)+ " successfully published")

#Called when message received on token_topic
# msg format = (node id)
def on_worker(client, userdata, msg):
    print("Received worker message " + str(msg.payload))
    global is_ended
    global is_working
    if (msg.payload == "stop"):
        is_ended = True
        print("Ended")
    elif (msg.payload == "wait"):
        time.sleep(3)
        is_working = False  # Request a new message
    else:
        read_range_message(client,msg.payload)

#Called when message received on will_topic
def on_will(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)

#Called when a message has been received on a subscribed topic (unfiltered)
def on_message(client, userdata, msg):
    print("Received message: "+str(msg.payload)+"on topic: "+msg.topic)
    print('unfiltered message')


#############################################
## Connect to broker and subscribe to topics
#############################################
try:
    # create a client instance
    client = mqtt.Client(str(UID), clean_session=True)

    # setup will for client
    will_message = str(UID)
    client.will_set(will_topic, will_message)

    # callbacks
    client.on_connect = on_connect
    client.on_publish = on_publish
    client.on_message = on_message

    # callbacks for specific topics
    client.message_callback_add(my_worker_topic, on_worker)
    client.message_callback_add(will_topic, on_will)

    # connect to broker
    client.connect(broker, port, keepalive=30)

    # subscribe to list of topics
    client.subscribe([(my_worker_topic, qos),
                      (will_topic, qos),
                      ])

    time.sleep(3)
    # initiate pub/sub
#    if UID == 1:
#        time.sleep(5)
#        client.publish(send_token_topic, UID)

    # network loop
    client.loop_start()

except (KeyboardInterrupt):
    print("Interrupt received")
except (RuntimeError):
    print("Runtime Error")
    client.disconnect()

while (not is_ended):
    if (not is_working):
        is_working = True
        client.publish(request_topic, str(UID))
    else:
        time.sleep(1)