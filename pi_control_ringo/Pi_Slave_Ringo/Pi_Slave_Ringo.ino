/*

Ringo Robot
Ringo_Base_Sketch_01
Version 2.0 8/2015

This is a basic sketch that can be used as a starting point
for various functionality of the Ringo robot.

Playing with Neo Pixels.
Ringo waits till a button in the remote is pressed.
The blinking rear pixel in blue signals that it is waiting
Once a button in the remote is pressed
Its eyes start to blink

*/

#include "RingoHardware.h"


void setup(){
  HardwareBegin();        //initialize Ringo's brain to work with his circuitry
  PlayStartChirp();       //Play startup chirp and blink eyes
  RxIRRestart(4);         //wait for 4 byte IR remote command
  IsIRDone();
  GetIRButton();
  SwitchMotorsToSerial(); //Call "SwitchMotorsToSerial()" before using Serial.print functions as motors & serial share a line
  RestartTimer();
}

void loop(){ 
  byte button;
/*  
  if(GetTime()>1000){   //blink rear pixel once a second to indicate Ringo is in "Menu" mode
      SetPixelRGB(0,0,0,100);   // turn on rear tail light to BLUE
      delay(10);                // brief delay
      OffPixels();              // turn off all pixels
      RestartTimer();           // zero timer
  }
*/   
  if(IsIRDone()){                   //wait for an IR remote control command to be received
    button = GetIRButton();       // read which button was pressed, store in "button" variable
    switch (button){
      case 1:
        //SetPixelRGB( EYE_RIGHT, 220, 30, 160);  //set pixel 4 (Right eye)
        //SetPixelRGB( EYE_LEFT, 220, 30, 160);  //set pixel 5 (Left eye)  
        OnEyes(1, 100, 0);
        //delay(100);
        break;
      case 2:
        //OffPixels();
        OnEyes(0, 0, 0);
        //delay(100);
        break;
        
    }
/*    
    while (1) {
        SetPixelRGB( EYE_RIGHT, 220, 30, 160);  //set pixel 4 (Right eye)
        SetPixelRGB( EYE_LEFT, 220, 30, 160);  //set pixel 5 (Left eye)  
        delay(100);
        SetPixelRGB( 4, 30, 220, 210);  //set pixel 4 (Right eye)
        SetPixelRGB( 5, 30, 220, 210);  //set pixel 5 (Left eye)  
        delay(100);
    }
*/    
  }
}











